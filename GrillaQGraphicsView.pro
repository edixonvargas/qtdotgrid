#-------------------------------------------------
#
# Project created by QtCreator 2012-06-06T20:53:58
#
#-------------------------------------------------

QT       += core gui

TARGET = GrillaQGraphicsView
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    scene.cpp

HEADERS  += mainwindow.h \
    scene.h

FORMS    += mainwindow.ui
