#include "scene.h"

Scene::Scene(QObject *parent) :
    QGraphicsScene(parent)
{
    //Establecemos el espaciado de la grilla
    space = 10;

    //Inicializamos la grilla con el espaciado indicado y la rellenamos de algun color
    pixmap = QPixmap(space, space);
    pixmap.fill(Qt::white);

    //Ahora dibujamos sobre el mapa de bits los dos puntos necesarios
    QPainter painter(&pixmap);
    painter.drawPoint(0, 0);
    painter.drawPoint(space, space);

}

void Scene::drawBackground(QPainter *painter, const QRectF &rect)
{
    //Esta instruccion es opcional y se usa si queremos guardar el estado del objeto painter
    painter->save();

    //Tomamos el origen del rectangulo visible
    double x = rect.x();
    double y = rect.y();

    //Crearemos una copia del rectangulo visible para poder modificarlo
    QRectF newRect(rect);

    //Ahora asignamos la esquina superior izquierda a un valor redondeado a nuestro espaciado de grilla
    newRect.setTopLeft(QPointF(floor(x/space)*space, floor(y/space)*space));
    painter->drawTiledPixmap(newRect, pixmap);

    //Podemos tambien dibujar un cruz en el centro si queremos
    painter->drawLine(-20, 0, 20, 0);
    painter->drawLine(0, -20, 0, 20);

    painter->end();
}
