#ifndef SCENE_H
#define SCENE_H

#include <QtGui>
#include <QtCore>

class Scene : public QGraphicsScene
{
        Q_OBJECT

        //Indica el espaciado de la grilla
        int space;

        //Contiene el mapa de bits
        QPixmap pixmap;

    public:
        explicit Scene(QObject *parent = 0);

    signals:

    public slots:

    protected:
        //Este metodo es llamado cada vez que se requiera pintar el fondo de QGraphicsScene
        void drawBackground(QPainter *painter, const QRectF &rect);
};

#endif // SCENE_H
